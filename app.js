import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import passport from 'passport';
import config from './src/config/config';

mongoose.Promise = global.Promise;

// * Routes
import Feed from './src/routes/post';
import Auth from './src/routes/auth';

// * Models
import './src/models/Users';

mongoose
  .connect(config.db, {
    useCreateIndex: true,
    useNewUrlParser: true
  })
  .then(console.log('Connected Successfully to MongoDB'))
  .catch(err => console.error(err));

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
require('./src/config/passport')(passport);

app.use('/sign', Auth);
app.use('/feed', passport.authenticate('jwt', { session: false }), Feed);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;
