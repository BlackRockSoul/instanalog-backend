const router = require('express').Router();

import passport from 'passport';

import Users from '../models/Users';
import Posts from '../models/Posts';
import Comments from '../models/Comments';
import NewsFeed from '../models/NewsFeed';

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      let follows = await NewsFeed.find({ user: req.user.id }).exec();
      let followings = follows.map(key => key.follow);
      followings.push(await Users.findById(req.user.id)); //Add user's posts to his feed
      let feed = await Posts.aggregate([
        //TODO Need to deal with DB model. Too hard to receive comments based on post and author based on comment.
        {
          $match: { author: { $in: followings } }
        },
        {
          $lookup: {
            from: 'users',
            localField: 'author',
            foreignField: '_id',
            as: 'author'
          }
        },
        { $unwind: '$author' },
        {
          $project: {
            author: { _id: '$author._id', userName: '$author.userName' },
            comments: 1,
            date: 1,
            post: 1,
            photo: 1,
            title: 1
          }
        },
        {
          $lookup: {
            from: 'comments',
            localField: '_id',
            foreignField: 'post',
            as: 'comments'
          }
        },
        { $unwind: { path: '$comments', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'users',
            localField: 'comments.author',
            foreignField: '_id',
            as: 'comments.author'
          }
        },
        {
          $unwind: {
            path: '$comments.author',
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $project: {
            author: {
              _id: '$author._id',
              userName: '$author.userName'
            },
            date: 1,
            post: 1,
            photo: 1,
            title: 1,
            comments: {
              _id: '$comments._id',
              text: '$comments.text',
              date: '$comments.date',
              author: {
                _id: '$comments.author._id',
                userName: '$comments.author.userName'
              }
            }
          }
        },
        {
          $group: {
            _id: '$_id',
            date: { $first: '$date' },
            author: { $first: '$author' },
            post: { $first: '$post' },
            comments: { $push: '$comments' },
            photo: { $first: '$photo' },
            title: { $first: '$title' }
          }
        },
        { $sort: { date: -1 } }
      ]).exec();
      res.status(200).json(feed);
    } catch (err) {
      console.error(err);
      res.status(400).json(err);
    }
  }
);

router.post(
  '/new',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let newPost;
    try {
      newPost = await Posts.create({
        author: req.user.id,
        title: req.body.title,
        photo: req.body.photo,
        post: req.body.post
      });
    } catch (err) {
      console.trace('Posts.create error');
      res.status(400).json(err);
    } finally {
      console.log(newPost);
      res.send();
    }
  }
);

router.post(
  '/comment/new',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let newComment;
    try {
      newComment = await Comments.create({
        author: req.user.id,
        post: req.body.post,
        text: req.body.text
      });
    } catch (err) {
      console.trace('Comments.create error');
      res.status(400).json(err);
    } finally {
      console.log(newComment);
      res.send();
    }
  }
);

export default router;
