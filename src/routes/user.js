/* eslint-disable no-undef */
const router = require('express').Router();

import User from '../models/Users';
import { genSalt, hash as _hash, compare } from 'bcryptjs';
import config from '../config/config';

import { sign } from 'jsonwebtoken';

router.post('/register', (req, res) => {
  User.findOne({
    emailAddress: req.body.emailAddress
  }).then(user => {
    if (user) {
      let error = 'Email Address Exists in Database.';
      return res.status(400).json(error);
    } else {
      const newUser = new User({
        name: req.body.name,
        emailAddress: req.body.emailAddress,
        password: req.body.password
      });
      genSalt(10, (err, salt) => {
        if (err) throw err;
        _hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => res.status(400).json(err));
        });
      });
    }
  });
});

router.get('/register1', (req, res) => {
  console.log(req, res);
});

router.post('/login', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  User.findOne({
    email
  }).then(user => {
    if (!user) {
      errors.email = 'No Account Found';
      return res.status(404).json(errors);
    }
    compare(password, user.password).then(isMatch => {
      if (isMatch) {
        const payload = {
          id: user._id,
          name: user.userName
        };
        sign(
          payload,
          config.secret,
          {
            expiresIn: 36000
          },
          (err, token) => {
            if (err)
              res.status(500).json({
                error: 'Error signing token',
                raw: err
              });
            res.json({
              success: true,
              token: `Bearer ${token}`
            });
          }
        );
      } else {
        errors.password = 'Password is incorrect';
        res.status(400).json(errors);
      }
    });
  });
});

export default router;
