/* eslint-disable no-undef */
const router = require('express').Router();

import User from '../models/Users';
import { genSalt, hash as _hash, compare } from 'bcryptjs';
import config from '../config/config';
import { sign } from 'jsonwebtoken';

router.post('/up', (req, res) => {
  User.findOne({
    emailAddress: req.body.emailAddress
  }).then(user => {
    if (user) {
      let error = 'Email Address Exists in Database.';
      return res.status(400).json(error);
    } else {
      const newUser = new User({
        userName: req.body.userName,
        emailAddress: req.body.emailAddress,
        password: req.body.password
      });
      genSalt(10, (err, salt) => {
        if (err) throw err;
        _hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then(user => {
              const payload = {
                id: user._id,
                name: user.userName
              };
              sign(
                payload,
                config.secret,
                {
                  expiresIn: 36000
                },
                (err, token) => {
                  if (err)
                    res.status(500).json({
                      error: 'Error signing token',
                      raw: err
                    });
                  res.json({
                    success: true,
                    token: `Bearer ${token}`
                  });
                }
              );
            })
            .catch(err => res.status(400).json(err));
        });
      });
    }
  });
});

router.get('/', (req, res) => {
  res.send(req, res);
});

router.post('/in', (req, res) => {
  const emailAddress = req.body.emailAddress;
  const password = req.body.password;
  const errors = {};
  if (!emailAddress || !password) {
    errors.email = 'E-Mail is incorrect!';
    res.status(400).json(errors);
  }
  User.findOne({
    emailAddress: emailAddress
  }).then(user => {
    if (!user) {
      errors.email = 'No Account Found';
      return res.status(404).json(errors);
    }
    compare(password, user.password).then(isMatch => {
      if (isMatch) {
        const payload = {
          id: user._id,
          name: user.userName
        };
        sign(
          payload,
          config.secret,
          {
            expiresIn: 36000
          },
          (err, token) => {
            if (err)
              res.status(500).json({
                error: 'Error signing token',
                raw: err
              });
            res.json({
              success: true,
              userName: user.userName,
              token: `Bearer ${token}`
            });
          }
        );
      } else {
        errors.password = 'Password is incorrect';
        res.status(400).json(errors);
      }
    });
  });
});

export default router;
