import { Schema, model } from 'mongoose';

const UserSchema = new Schema({
  name: { type: String },
  emailAddress: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  userName: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

export default model('User', UserSchema);
