import { Schema, model } from 'mongoose';

const NewsFeed = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  follow: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  }
});

export default model('NewsFeed', NewsFeed);
