import { Schema, model } from 'mongoose';

const Comments = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  text: {
    type: String,
    required: true,
    default: '',
    trim: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

export default model('Comments', Comments);
