import { Schema, model } from 'mongoose';

const Posts = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  post: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  comments: {
    type: Schema.Types.ObjectId,
    ref: 'Comments'
  }
});

export default model('Posts', Posts);
