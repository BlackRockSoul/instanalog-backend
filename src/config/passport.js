import { Strategy, ExtractJwt } from 'passport-jwt';
import config from './config';

import User from '../models/Users';

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.secret
};

module.exports = passport => {
  passport.use(
    new Strategy(opts, (payload, done) => {
      User.findById(payload.id, (err, user) => {
        if (user) {
          return done(null, {
            id: user.id,
            name: user.userName,
            email: user.emailAddress
          });
        }
        return done(null, false);
      }).catch(err => console.error(err));
    })
  );
};
